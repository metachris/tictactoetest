import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by chris on 19/10/15.
 */
public class GameState {
    int[] fields = new int[9];

    final static int FIELD_EMPTY = 0;
    final static int PLAYER_X = 1;
    final static int PLAYER_O = 2;

    /**
     * Create default empty GameState instance
     */
    public GameState() {
        this.fields = new int[]{
                FIELD_EMPTY, FIELD_EMPTY, FIELD_EMPTY,
                FIELD_EMPTY, FIELD_EMPTY, FIELD_EMPTY,
                FIELD_EMPTY, FIELD_EMPTY, FIELD_EMPTY
            };
    }

    @Override
    public String toString() {
        return "GameState{" +
                "fields=" + Arrays.toString(this.fields) +
                "}, winner=" + this.getWinner() +
                ", isComplete=" + this.isComplete() +
                ", isDraw=" + this.isDraw();
    }

    public ArrayList<Integer> getPossibleMoves() {
        ArrayList<Integer> possibleMoves = new ArrayList<Integer>();
        for (int i=0; i<fields.length; i++) {
            if (fields[i] == FIELD_EMPTY) {
                possibleMoves.add(i);
            }
        }
        return possibleMoves;
    }

    /**
     * @return null if no winner, 1 if player1, 2 if player2
     */
    public Integer getWinner() {
        // TODO: Add getWinner logic!
        // if (field[0] == PLAYER_X && field[1] == PLAYER_X && field[2] == PLAYER_X) ...
        int winner = 0;
        if (winner == PLAYER_X) {
            return PLAYER_X;
        } else if (winner == PLAYER_O) {
            return PLAYER_O;
        } else {
            return null;
        }
    }

    /**
     * Complete means either
     * -) no moves left
     * -) or someone has won
     */
    public boolean isComplete() {
        return getPossibleMoves().size() == 0 || getWinner() != null;
    }

    /**
     * Draw means
     * - no moves left
     * - and no winner
     */
    public boolean isDraw() {
        return getPossibleMoves().size() == 0 && getWinner() == null;
    }

    public boolean playerXWon() {
        Integer winner = getWinner();
        return winner != null && winner == PLAYER_X;
    }

    public boolean playerOWon() {
        Integer winner = getWinner();
        return winner != null && winner == PLAYER_O;
    }

    public static int getEnemy(int player) {
        if (player == PLAYER_X) {
            return PLAYER_O;
        } else {
            return PLAYER_X;
        }
    }

    /**
     * @param position 0..8
     * @param state PLAYER_X, PLAYER_O or FIELD_EMPTY
     */
    public void makeMove(int position, int state) {
        fields[position] = state;
    }
}
