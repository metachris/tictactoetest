import java.util.ArrayList;

/**
 * PlayerX = Human
 * PlayerO = Computer
 */
public class TicTacToe {
    GameState state = new GameState();

    @Override
    public String toString() {
        return state.toString();
    }

    public void makeBestNextMove(int player, int recursionLevels) {
        Integer bestMove = null;
        Integer bestMoveScore = null;

        // Evaluate all moves
        for (Integer move : state.getPossibleMoves()) {
            // Test this move
            state.makeMove(move, player);

            // Evaluate consequences (with all possible enemy moves and so on)
            System.out.println("");
            int val = minimax(state, GameState.getEnemy(player), recursionLevels);

            // Undo move
            state.makeMove(move, GameState.FIELD_EMPTY);

            // Remember best scored move
            if (player == GameState.PLAYER_X) {
                // Negative scores are good for player X
                if (bestMoveScore == null || val < bestMoveScore) {
                    bestMoveScore = val;
                    bestMove = move;
                }
            } else {
                // Positive scores are good for player X
                if (bestMoveScore == null || val > bestMoveScore) {
                    bestMoveScore = val;
                    bestMove = move;
                }
            }
        }

        // Make the move deemed best
        state.makeMove(bestMove, player);
    }

    /**
     * Get best for the given player (GameState.PLAYER_X or GameState.PLAYER_O)
     *
     * * negative values are good for PLAYER_X
     * * positive values are good for PLAYER_O
     */
    public int minimax(GameState state, int player, int recursionLevels) {
        System.out.println("minimax: state=" + state.toString() + ", player=" + player + ", rec=" + recursionLevels);

        // Return now if game already ended
        if (state.isComplete()) {
            if (state.playerXWon()) {
                return -10;
            } else if (state.playerOWon()) {
                return 10;
            } else {
                // tie
                return 0;
            }
        }

        if (recursionLevels < 1) {
            return 0;
        }

        Integer bestMove = null;
        Integer bestMoveScore = null;

        // Try all possible moves in this iteration
        for (Integer move : state.getPossibleMoves()) {
            // Build dummy gameState to make test move on
            state.makeMove(move, player);

            // RECURSION HERE. Go next step in the evaluation tree (until recursionLevels hits 0)
            int recursionLevelsLeft = recursionLevels - 1;
            int val = minimax(state, GameState.getEnemy(player), recursionLevelsLeft);

            // Undo move
            state.makeMove(move, GameState.FIELD_EMPTY);

            // Remember best scored move
            if (player == GameState.PLAYER_X) {
                // Negative scores are good for player X
                if (bestMoveScore == null || val < bestMoveScore) {
                    bestMoveScore = val;
                    bestMove = move;
                }
            } else {
                // Positive scores are good for player O
                if (bestMoveScore == null || val > bestMoveScore) {
                    bestMoveScore = val;
                    bestMove = move;
                }
            }
        }

        return bestMoveScore;
    }

}
