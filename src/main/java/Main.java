/**
 * Created by chris on 19/10/15.
 */
public class Main {
    public static void main(String[] args) {
        TicTacToe tic = new TicTacToe();
        System.out.println(tic.toString());
        tic.makeBestNextMove(GameState.PLAYER_X, 3);
        System.out.println(tic.toString());
    }
}
